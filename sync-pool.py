"""
Copyright  2021-2022 Matograine <matograine(AT)zaclys(POINT)net>

Sync-pool is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sync-pool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Sync-pool. If not, see <https://www.gnu.org/licenses/>.
"""

import sys
import requests
import json
import time

from duniterpy.documents.identity import Identity
from duniterpy.documents.certification import Certification
from duniterpy.documents.revocation import Revocation
from duniterpy.documents.membership import Membership

###### Parameters ######

SLEEP_TIME = 1
PAUSE_TIME = 60


###### Functions ######


def create_members_list(node, members_list):
    print("Getting members...")
    url = node + "/wot/members/"
    members = requests.get(url).text
    members = json.loads(members)["results"]
    for m in members:
        members_list.append(m["pubkey"])


def get_willmembers(node, pubkey, members_list, willmembers, pending_rev):
    url = node + "/wot/lookup/" + pubkey
    lookup = requests.get(url).text
    #    print (lookup)
    lookup = json.loads(lookup)["results"]
    for result in lookup:
        if result["pubkey"] == pubkey:
            for receiver in result["signed"]:
                pk = receiver["pubkey"]
                if not (pk in members_list or pk in willmembers):
                    willmembers.append(pk)


def get_pending_docs(node, pubkey, ms, certs, ids, revs):
    req = get_requirements(node, pubkey)


def get_requirements(node, pubkey):
    while true:
        try:
            url = node + "/wot/requirements/" + pubkey
            req = requests.get(url).text
            return json.loads(req)
        except:
            time.sleep(PAUSE_TIME)


def send_doc(doc, nodes, pubkey):
    typ = type(doc)
    if typ == Identity:
        url_path = "/wot/add"
        param = "identity"
    elif typ == Certification:
        url_path = "/wot/certify"
        param = "cert"
    elif typ == Revocation:
        url_path = "/wot/revoke/"
        param = "revocation"
    elif typ == Membership:
        url_path = "/blockchain/membership"
        param = "membership"

    data = {param: doc.signed_raw()}

    for node in nodes:
        url = node + url_path
        a = 0
        while a < 5:
            req = requests.post(url, data)
            if req.status_code == 200:
                break
            elif req.status_code == 429:
                time.sleep(PAUSE_TIME)
                a += 1
            else:
                res = load_response(req)
                print (res["ucode"], ":", res["message"])
                print ("URL:", url, "\nDoc:", param, pubkey)
                break


def load_response(response):
    res = response.text
    return json.loads(res)


def create_id(id, currency):
    return Identity(
        version=10,
        currency=currency,
        pubkey=id["pubkey"],
        uid=id["uid"],
        ts=id["meta"]["timestamp"],
        signature=id["sig"],
    )


def create_membership(membership, id, currency):
    return Membership(
        version=10,
        currency=currency,
        issuer=id.pubkey,
        membership_ts=membership["blockstamp"],
        membership_type=membership["membership"],
        uid=id.uid,
        identity_ts=id.timestamp,
        signature=membership["sig"],
    )


def create_revoke(sig, id, currency):
    return Revocation(
        version=10,
        currency=currency,
        identity=id,
        signature=sig,
    )


def create_cert(id, cert, currency):
    return Certification(
        version=10,
        currency=currency,
        pubkey_from=cert["from"],
        identity=id,
        timestamp=cert["blockstamp"],
        signature=cert["sig"],
    )


def get_currency(node):
    url = node + "/blockchain/parameters"
    req = requests.get(url).text
    req = json.loads(req)
    return req["currency"]


def propagate_docs_member(
    member, members_list, willmembers_list, source_node, target_nodes
):
    # get currency
    currency = get_currency(source_node)
    # get requirements
    url = source_node + "/wot/requirements/" + member
    req = requests.get(url).text
    req = json.loads(req)
    id = req["identities"][0]
    # from requirements :
    # create identity
    identity = create_id(id, currency)
    # create pending membership, revocation if needed
    if id["pendingMemberships"]:
        for membership in id["pendingMemberships"]:
            membership = create_membership(membership, identity, currency)
            send_doc(membership, target_nodes, member)
    if id["revocation_sig"]:
        revoke = create_revoke(id["revocation_sig"], identity, currency)
        send_doc(revoke, target_nodes, member)
    # create pending certifications
    if id["pendingCerts"]:
        for cert in id["pendingCerts"]:
            cert = create_cert(identity, cert, currency)
            send_doc(cert, target_nodes, member)
    # get lookup
    url_lookup = source_node + "/wot/lookup/" + member
    lookup = requests.get(url_lookup).text
    lookup = json.loads(lookup)
    id_lookup = lookup["results"][0]
    # loop through sent certifications. Add non-members to willmembers list.
    for cert in id_lookup["signed"]:
        pk = cert["pubkey"]
        if not (pk in members_list or pk in willmembers_list):
            willmembers_list.append(pk)
    time.sleep(SLEEP_TIME)


def propagate_docs_willmember(willmember, source_node, target_nodes):
    # get currency
    currency = get_currency(source_node)
    # get requirements
    url = source_node + "/wot/requirements/" + willmember
    req = requests.get(url).text
    req = json.loads(req)
    identities = req["identities"]
    # from requirements :
    # create identity
    for id in identities:
        identity = create_id(id, currency)
        send_doc(identity, target_nodes, willmember)
        # create pending membership, revocation if needed
        if id["pendingMemberships"]:
            for membership in id["pendingMemberships"]:
                membership = create_membership(membership, identity, currency)
                send_doc(membership, target_nodes, willmember)
        if id["revocation_sig"]:
            revoke = create_revoke(id["revocation_sig"], identity, currency)
            send_doc(revoke, target_nodes, willmember)
        # create pending certifications
        if id["pendingCerts"]:
            for cert in id["pendingCerts"]:
                cert = create_cert(identity, cert, currency)
                send_doc(cert, target_nodes, willmember)
    time.sleep(SLEEP_TIME)


def sort_nodes(source_node):
    nodes_list = list()
    for node in NODES:
        if not source_node in node:
            nodes_list.append(node)
    return nodes_list


###### main script ######


def main():

    source_node = sys.argv[1]
    target_nodes = list()
    for node in sys.argv:
        if not (node is sys.argv[1] or node is sys.argv[0]):
            target_nodes.append(node)

    # create members list
    members_list = []
    willmembers_list = []
    pending_certs_list = []
    pending_ms_list = []
    pending_id_list = []
    pending_rev_list = []

    create_members_list(source_node, members_list)
    print("Members number :", len(members_list))

    print("Propagating members documents")
    for member in members_list:
        propagate_docs_member(
            member, members_list, willmembers_list, source_node, target_nodes
        )

    print("Propagating willMembers documents")
    print("WillMembers number:", len(willmembers_list))
    for willmember in willmembers_list:
        propagate_docs_willmember(willmember, source_node, target_nodes)

    print("Members number :", len(members_list))
    print("WillMembers number:", len(willmembers_list))


main()

