# Sync-pools

Sync-pool is a Python script that aims at syncing poole from one Duniter node to many.
It only syncs docs for identities that have at least one valid certification.
It can be used to force the sync of pools after a node blockchain sync.
It uses BMA interface.

I created it because I never found how to use duniters --crawl-lookup feature.

## Use

> python3 sync-pools.py <source_node> <target_node_1> <target_node_2> <...>

Caution: indicate protocol and ports. ex:

> python3 ./sync-pool.py https://g1.duniter.org:443 https://g1.presles.fr:443

## Dependencies
You should have installed:
Duniterpy 0.61.0

(may work with upper versions, though)

## Should you use it ?

1- if you want to sync _your_ node's mempools, sure.

2- if you want to sync other nodes, be sure that nothing similar is in place. We don't want to DDOS them ;-)
I (matograine) run it weekly to sync these node's mempools:

* https://g1.duniter.org:443
* https://g1.presles.fr:443
* https://monit.g1.nordstrom.duniter.org/bma
* http://g1.monnaielibreoccitanie.org:80
